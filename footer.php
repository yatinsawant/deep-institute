<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="widget">
                    <h5 class="widgetheading">Our Contact</h5>

                    <address>
                        <strong style="font-size: 20px">
                            Deep Institute
                        </strong><br>
                        Office No1, Ground Floor, Star Trade Center,<br>
                        Near Chamunda Circle, Sodawala Lane, <br>
						S.V.P. Road, Borivali-W, Mumbai-400092.
                    </address>

                    <p><i class="icon-phone"></i> 
                        08888 977 933<br>
                        <i class="icon-envelope-alt"></i>
                        info@higherconcept.in</p>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Quick Links</h5>

                    <ul class="link-list">
                        <li>
                            <a href="#">Latest Events</a>
                        </li>

                        <li>
                            <a href="#">Terms and conditions</a>
                        </li>

                        <li>
                            <a href="#">Privacy policy</a>
                        </li>

                        <li>
                            <a href="careers.php">Careers</a>
                        </li>

                        <li>
                            <a href="#">Contact us</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Latest posts</h5>

                    <ul class="link-list">
                        <li>
                            <a href="#"></a>
                        </li>

                        <li>
                            <a href="#"></a>
                        </li>

                        <li>
                            <a href="#"></a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Recent News</h5>

                    <ul class="link-list">
                        <li>
                            <a href="#"></a>
                        </li>

                        <li>
                            <a href="#"></a>
                        </li>

                        <li>
                            <a href="#"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="copyright">
                        <p>
                            <span>Designed and developed by</span> <a href="http://www.higherconcept.in">Higher Concept</a>
                            <span>&copy; 2017 All right reserved.</span>
                        </p>
                    </div>
                </div>

                <div class="col-lg-6">
                    <ul class="social-network">
                        <li>
                            <a class="fa fa-facebook" data-placement=
                               "top" href="#" style="font-style: italic"
                               title="Facebook"></a>
                        </li>

                        <li>
                            <a class="fa fa-twitter" data-placement=
                               "top" href="#" style="font-style: italic"
                               title="Twitter"></a>
                        </li>

                        <li>
                            <a class="fa fa-linkedin" data-placement=
                               "top" href="#" style="font-style: italic"
                               title="Linkedin"></a>
                        </li>

                        <li>
                            <a class="fa fa-pinterest" data-placement=
                               "top" href="#" style="font-style: italic"
                               title="Pinterest"></a>
                        </li>

                        <li>
                            <a class="fa fa-google-plus"
                               data-placement="top" href="#" style=
                               "font-style: italic" title=
                               "Google plus"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<script data-skip-moving="true">
        (function(w,d,u,b){
                s=d.createElement('script');
				r=(Date.now()/1000|0);
				s.async=1;
				s.src=u+'?'+r;
                //h=d.getElementsByTagName('script')[0];
				h=d.getElementById('script_id');
				h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.com/b3430103/crm/site_button/loader_2_xjreip.js');
</script>





